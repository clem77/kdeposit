'use strict';
const config    = require('./config/default');
const fileUtil  = require('./lib/fileUtil');
const models    = require('./models');

/**
 * MAIN
 */
(async function() {
    // Connect to the DB and sync models
    await models.init(config.db);

    // get list of json files in the data directory
    const files = await fileUtil.getJsonFiles(config.dataDir);

    // Extract all transactions from the files
    const transactions = await fileUtil.extractTxList(config.dataDir, files);

    // Save all the transactions in DB
    await models.saveTxList(transactions);

    // get deposit summary for each user
    const userDeposits = await models.findUserDeposit();
    for (let deposit of userDeposits) {
        printUserSummary(deposit.dataValues);
    }

    // get deposit summary for non-user
    const nonUserDeposit = await models.findNonUserDeposit();
    printNonUserSummary(nonUserDeposit);

    // get deposit min amount
    const minDeposit = await models.Transaction.findMinDepositAmount();
    console.log('Smallest valid deposit:', minDeposit.toFixed(8));

    // get deposit max amount
    const maxDeposit = await models.Transaction.findMaxDepositAmount();
    console.log('Largest valid deposit:', maxDeposit.toFixed(8));

    process.exit(0);
})()
    .catch(err => {
        console.error(err);
        process.exit(1);
    });


// log user stats in Stdout
function printUserSummary ({ firstname, middlename, lastname, txCount, totalAmount }) {
    let fullname = firstname;
    fullname += middlename ? (' ' + middlename) : '';
    fullname += lastname ? (' ' + lastname) : '';
    console.log(`Deposited for ${fullname}: count=${txCount ? txCount : 0} sum=${totalAmount ? totalAmount.toFixed(8) : 0}`)
}

// log non-user stats in Stdout
function printNonUserSummary (addressDetails) {
    let txCount = 0;
    let totalAmount = 0;
    for (let detail of addressDetails) {
        txCount += parseInt(detail.dataValues.txCount);
        totalAmount += parseFloat(detail.dataValues.totalAmount);
    }
    console.log(`Deposited without reference: count=${txCount ? txCount : 0} sum=${totalAmount ? totalAmount.toFixed(8) : 0}`)
}

