'use strict';

const expect = require('chai').expect;

describe('models/index', () => {
    const models = require('../models/index');

    before(() => {
        return models.init({
            dialect: "sqlite",
            storage: ":memory:",
            operatorsAliases: false,
            logging: false
        });
    });

    describe('models creation', () => {

        it('returns the address model', () => {
            expect(models.Address).to.be.ok;
        });

        it('returns the transaction model', () => {
            expect(models.Transaction).to.be.ok;
        });

        it('returns the user model', () => {
            expect(models.User).to.be.ok;
        });
    });

    describe('findUserDeposit', () => {

        before(async () => {
            let bob = await models.User.create({
                firstname: 'Bob',
                middlename: 'John',
                lastname: 'Smith'
            });

            let alice = await models.User.create({
                firstname: 'Alice'
            });

            let bobAddress =  await models.Address.create({
                address: 'addr1234'
            });
            bobAddress.setUser(bob);

            let bobTx1 = await models.Transaction.create({
                amount: 3.0,
                confirmations: 2,
                txid: 'a1234'
            });
            bobTx1.setAddress(bobAddress);

            let bobTx2 = await models.Transaction.create({
                amount: 1.1,
                confirmations: 6,
                txid: 'b1234'
            });
            bobTx2.setAddress(bobAddress);

            let bobTx3 = await models.Transaction.create({
                amount: 3.00000001,
                confirmations: 7,
                txid: 'c1234'
            });
            bobTx3.setAddress(bobAddress);

        });

        // clean up
        after(async () => {
            await models.User.destroy({where: {}});
            await models.Address.destroy({where: {}});
            await models.Transaction.destroy({where: {}});
        });

        it('verify that the right number of user are in db', () => {
            return models.User.findAll()
                .then(users => expect(users).to.have.lengthOf(2))
        });

        it('return correct deposit amounts for users', () => {
            return models.findUserDeposit()
                .then(results => {
                    for (let r of results) {
                        if (r.dataValues.firstname === 'Bob') {
                            return r.dataValues.totalAmount;
                        }
                    }
                })
                .then(bobDepositAmount => {
                    expect(bobDepositAmount.toFixed(8)).to.equal('4.10000001');
                });
        });

    });

});