'use strict';
const fs        = require('fs');
const util      = require('util');
const readdir   = util.promisify(fs.readdir); // require nodeJs 8+
const readFile  = util.promisify(fs.readFile); // require nodeJs 8+

const fileUtil  = {};

// get list of json files in the data directory
fileUtil.getJsonFiles = async function(dataDir) {
  let files = await readdir(dataDir);
  files = files.filter(file => file.slice(-5) === '.json');
  return files
};


// read and parse the content of each file
fileUtil.extractTxList = async function(dataDir, files) {
  let txs = [];
  for (let file of files) {
    try {
      let rawContent = await readFile(dataDir + file, 'utf8');
      let parsedContent = JSON.parse(rawContent);
      _validateContent(parsedContent);
      txs = [...txs, ...parsedContent.transactions]
    } catch (e) {
      throw Error(`Fail to read and parse ${file} content. ${e}`);
    }
  }
  return txs
};

// helper - check that a file content contains "transactions"
function _validateContent(content) {
  if (!content || !content.transactions)
    throw Error('Invalid content: no key transactions');
  if (!Array.isArray(content.transactions))
    throw Error('Invalid content: transactions is not an array');
}

module.exports = fileUtil;