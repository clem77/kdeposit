'use strict';
const path = require('path');

module.exports = {
  dataDir: process.env.DATA_DIR || path.join(__dirname, "../data/"),
  db: {
      username: process.env.DB_USERNAME || 'docker',
      password: process.env.DB_PASSWORD || 'docker',
      database: process.env.DB_DATABASE || 'docker',
      host: process.env.DB_HOST || '127.0.0.1',
      port: process.env.DB_PORT || '5432',
      dialect: 'postgres',
      operatorsAliases: false,
      logging: false,
      pool: {
          max: 5,
          min: 0,
          acquire: 30000,
          idle: 10000
      }
  }
};
