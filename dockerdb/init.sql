
CREATE TABLE "User" (
  id            SERIAL          PRIMARY KEY
, firstname     VARCHAR(255)    NOT NULL
, middlename    VARCHAR(255)
, lastname      VARCHAR(255)
);

CREATE TABLE "Address" (
  id        SERIAL          PRIMARY KEY
, address   VARCHAR(255)    UNIQUE NOT NULL
, userid    INTEGER REFERENCES "User" ("id") ON DELETE SET NULL ON UPDATE CASCADE
);

INSERT INTO "User" (firstname, lastname) VALUES ('Wesley','Crusher');
INSERT INTO "User" (firstname, lastname) VALUES ('Leonard','McCoy');
INSERT INTO "User" (firstname, lastname) VALUES ('Jonathan','Archer');
INSERT INTO "User" (firstname, lastname) VALUES ('Jadzia','Dax');
INSERT INTO "User" (firstname, lastname) VALUES ('Montgomery','Scott'); -- reverse firstname and lastname?
INSERT INTO "User" (firstname, middlename, lastname) VALUES ('James', 'T.','Kirk');
INSERT INTO "User" (firstname) VALUES ('Spock');

INSERT INTO "Address" (address, userid) VALUES ('mvd6qFeVkqH6MNAS2Y2cLifbdaX5XUkbZJ', 1);
INSERT INTO "Address" (address, userid) VALUES ('mmFFG4jqAtw9MoCC88hw5FNfreQWuEHADp', 2);
INSERT INTO "Address" (address, userid) VALUES ('mzzg8fvHXydKs8j9D2a8t7KpSXpGgAnk4n', 3);
INSERT INTO "Address" (address, userid) VALUES ('2N1SP7r92ZZJvYKG2oNtzPwYnzw62up7mTo', 4);
INSERT INTO "Address" (address, userid) VALUES ('mutrAf4usv3HKNdpLwVD4ow2oLArL6Rez8', 5);
INSERT INTO "Address" (address, userid) VALUES ('miTHhiX3iFhVnAEecLjybxvV5g8mKYTtnM', 6);
INSERT INTO "Address" (address, userid) VALUES ('mvcyJMiAcSXKAEsQxbW9TYZ369rsMG6rVV', 7);