'use strict';
module.exports = (sequelize, DataTypes) => {
    let User = sequelize.define('User', {
        firstname     : DataTypes.STRING,
        middlename    : DataTypes.STRING,
        lastname      : DataTypes.STRING,
    }, {
        timestamps: false,
        freezeTableName: true,
    });

    User.associate = function (models) {
        models.User.hasOne(models.Address, {foreignKey: 'userid'});
    };

    return User;
};