'use strict';
module.exports = (sequelize, DataTypes) => {
    let Address = sequelize.define('Address', {
        address       : DataTypes.STRING,
    },{
        timestamps: false,
        freezeTableName: true,
    });

    Address.associate = function (models) {
        Address.hasMany(models.Transaction, {as: 'Transactions'});
        Address.belongsTo(models.User, {foreignKey: 'userid'} );
    };


    return Address;
};