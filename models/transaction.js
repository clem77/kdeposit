'use strict';
const Op = require('sequelize').Op;

module.exports = (sequelize, DataTypes) => {
    let Transaction = sequelize.define('Transaction', {
        amount        : DataTypes.FLOAT,
        confirmations : DataTypes.INTEGER,
        txid          : {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
    }, {
        timestamps: false,
        freezeTableName: true,
    });

    Transaction.associate = function (models) {
        models.Transaction.belongsTo(models.Address);
    };

    Transaction.findMaxDepositAmount = async function() {
        return await Transaction.max('amount', {
            where: {
                confirmations: {[Op.gte]: 6},
                amount: {[Op.gt]: 0}
            }
        })
    };

    Transaction.findMinDepositAmount = async function() {
        return await Transaction.min('amount', {
            where: {
                confirmations: {[Op.gte]: 6},
                amount: {[Op.gt]: 0}
            }
        })
    };

    return Transaction;
};


