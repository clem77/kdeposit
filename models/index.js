'use strict';

const fs        = require('fs');
const path      = require('path');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const basename  = path.basename(__filename);

let models = {};

// Connect to the db and sync the models
models.init = async function(config) {
    models.sequelize = await _establishConnection(config, 0);

    fs.readdirSync(__dirname)
        .filter(file => {
            return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
        })
        .forEach(file => {
            let model = models.sequelize['import'](path.join(__dirname, file));
            models[model.name] = model;
        });

    Object.keys(models).forEach(modelName => {
        if (models[modelName].associate) {
            models[modelName].associate(models);
        }
    });

    await models.sequelize.sync({force: false});
};

// find transactions count + total amount for each user
models.findUserDeposit = async function() {
    return await models.User.findAll({
        attributes: [
            'id',
            'firstname',
            'middlename',
            'lastname',
            [models.sequelize.fn('count', models.sequelize.col('Address->Transactions.id')), 'txCount'],
            [models.sequelize.fn('sum', models.sequelize.col('Address->Transactions.amount')), 'totalAmount']
        ],
        include: [
            {
                model: models.Address,
                as: 'Address',
                attributes: ['address'],
                include: [
                    {
                        model: models.Transaction,
                        as: 'Transactions',
                        attributes: [],
                        where: {
                            confirmations: {[Op.gte]: 6},
                            amount: {[Op.gt]: 0}
                        }
                    }
                ]
            }
        ],
        group: [models.sequelize.col('Address.id'), models.sequelize.col('User.id')],
        order: models.sequelize.col('id')
    })
};

// find transactions sum and count for each non-user address
models.findNonUserDeposit = async function() {
    return await models.Address.findAll({
        attributes: [
            [models.sequelize.fn('count', models.sequelize.col('Transactions.id')), 'txCount'],
            [models.sequelize.fn('sum', models.sequelize.col('Transactions.amount')), 'totalAmount']
        ],
        include: [
            {
                model: models.Transaction,
                as: 'Transactions',
                attributes: [],
                where: {
                    confirmations: {[Op.gte]: 6},
                    amount: {[Op.gt]: 0}
                }
            }
        ],
        where: {
            userid: null
        },
        group: [models.sequelize.col('Address.id'), models.sequelize.col('Address.userid')],
    })
};

//
models.saveTxList = async function (transactions) {
    for (let transaction of transactions) {

        // only save deposits
        if (transaction.amount < 0) {
            continue;
        }

        // only keep 8 decimals
        transaction.amount = Math.floor(transaction.amount * 1e8) / 1e8;

        let addr =  await models.Address
            .findOrCreate({
                where: {
                    address: transaction.address
                },
            })
            .spread((address, _) => address);

        let { tx, wasCreated } = await models.Transaction
            .findOrCreate({
                where: {
                    txid: transaction.txid
                },
                defaults: transaction
            })
            .spread((tx, wasCreated) => {
                return { tx, wasCreated };
            });

        if (wasCreated) {
            await tx.setAddress(addr)
        }
    }
};


// helper - simple promise timeout
function _timeout(ms){
    return new Promise(res => setTimeout(res, ms))
}

// recursive function to establish connection with the DB
function _establishConnection(config, attempt) {
    const sequelize = new Sequelize(config);
    return sequelize.authenticate()
        .then(() => {
            // console.log('Connection has been established successfully.');
            return sequelize;
        })
        .catch(err => {
            if (attempt > 10) {
                throw Error(`Fail to connect to DB, Error: ${err}`)
            }
            return _timeout(1000).then(() => _establishConnection(config, attempt+1));
        });
}

module.exports = models;
